



[TOC]





# 1.MySQL 介绍

![1586656146783](1586656146783.png)



## 1.1MySQL 引擎

![1586656306785](1586656306785.png)









# 2.MySQL 系统命令



## 2.1服务管理

![1586656488242](1586656488242.png)



## 2.2登录MySQL

![1586656557747](1586656557747.png)

## 2.3控制台

![1586656589623](1586656589623.png)

![1586656607799](1586656607799.png)



## 2.4元数据

![1586656656417](1586656656417.png)



## 2.5配置文件

![1586656698949](1586656698949.png)





# 3.常用SQL



## 3.1 语法组成

![1586656766858](1586656766858.png)



## 3.2数据库操作

![1586656876097](1586656876097.png)

![1586656890762](1586656890762.png)

## 3.3数据库-表操作



### 3.3.1 查看表结构

![1586656953079](1586656953079.png)



### 3.3.2创建表

> create table 表名(
> 字段名1 类型[(宽度) 约束条件],
> 字段名2 类型[(宽度) 约束条件],
> 字段名3 类型[(宽度) 约束条件]
> ) 存储引擎 字符集 校对集;



### 3.3.3 删除表



> drop table 表名 ;



### 3.3.4 修改表及表字段结构

- 修改表名

> ALTER TABLE 旧表名 RENAME 新表名

- 修改表字符集

> ALTER TABLE t_eng_new DEFAULT CHARACTER SET 字符集 COLLATE  排序集

- 修改表引擎

> ```
> show engines;
> 
> # 直接修改会很慢 推荐 建新表 再插入
> ALTER TABLE t_eng_new ENGINE=innodb ;
> 
> ```



- 添加字段

>  ALTER TABLE <表名> ADD <新字段名> <数据类型> [约束条件] [FIRST|AFTER 已存在的字段名]；

- 修改字段

> alter table 表名 modify 字段名 字段类型 约束条件 [first|after 已存在的字段名]
>
> alter table 表名 change 旧字段名 新字段名 字段类型 约束条件 [first|after ]

- 删除字段

> alter table 表名 drop 字段名



### 3.3.4数据类型



![1586659755890](1586659755890.png)

![1586659788707](1586659788707.png)



### 3.3.5 自增

![1586659857986](1586659857986.png)



### 3.3.6 小数类型

![1586660801151](1586660801151.png)



### 3.3.7字符类型

![1586660826172](1586660826172.png)



### 3.3.8 日期类型

![1586660859749](1586660859749.png)





## 3.4临时表

![1586661231563](1586661231563.png)

### 3.4.1 临时表创建

![1586661262823](1586661262823.png)



### 3.4.2 临时表的操作

![1586661292189](1586661292189.png)



# 4.日志操作



## 4.1日志分类

![1586671917768](1586671917768.png)







### 4.1.1 错误日志

![1586672177122](1586672177122.png)



### 4.1.2慢查询日志

![1586672197792](1586672197792.png)







### 4.1.3日志分析工具

![1586672218167](1586672218167.png)

![1586672370204](1586672370204.png)