> 本文基于1.8 调试

@[toc]
## Map 继承关系图

![在这里插入图片描述](https://img-blog.csdnimg.cn/20190916161205111.png?x-oss-process=image/watermark,type_ZmFuZ3poZW5naGVpdGk,shadow_10,text_aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L3FxXzM1NTMwMDQy,size_16,color_FFFFFF,t_70)

## 创建Map 
Map 有四个构造函数
+ public HashMap() ;
+ public HashMap(int initialCapacity) ;
+ public HashMap(int initialCapacity, float loadFactor) ;
+ public HashMap(Map<? extends K, ? extends V> m) ;

```java

    /**
     * The load factor for the hash table.
     * 哈希表的加载因子。
     * @serial
     */
    final float loadFactor;
    
     /**
     * The next size value at which to resize (capacity * load factor).
     * 要调整大小的下一个大小阈值（根据 容量*加载因子得到 ）。
     * @serial
     */
    // (The javadoc description is true upon serialization.
    // Additionally, if the table array has not been allocated, this
    // field holds the initial array capacity, or zero signifying
    // DEFAULT_INITIAL_CAPACITY.)
    int threshold;
      /**
      在构造函数中未指定时使用的加载因子。
     * The load factor used when none specified in constructor.
     */
    static final float DEFAULT_LOAD_FACTOR = 0.75f;
  	/**
  		最大容量，如果隐式指定更高的值，则使用该容量
     * The maximum capacity, used if a higher value is implicitly specified
     * by either of the constructors with arguments.
     * MUST be a power of two <= 1<<30.
     */
    static final int MAXIMUM_CAPACITY = 1 << 30;
  
    /**
    	空参构造 默认初始容量 16 和 默认的扩容因子(负载因子)0.75 
     * Constructs an empty <tt>HashMap</tt> with the default initial capacity
     * (16) and the default load factor (0.75).
     */
    public HashMap() {
	    // 所有其他变量都是默认的 
	    // 这里使用默认的 0.75
        this.loadFactor = DEFAULT_LOAD_FACTOR; // all other fields defaulted
    }
   /**
   使用指定的初始值 构造一个空的 HashMap和默认负载系数（0.75）。
     * Constructs an empty <tt>HashMap</tt> with the specified initial
     * capacity and the default load factor (0.75).
     *
     * @param  initialCapacity the initial capacity.  initialCapacity初始容量。
     * @throws IllegalArgumentException if the initial capacity is negative.
     */
    public HashMap(int initialCapacity) {
	    // 使用指定的初始容量 和默认的加载因子
        this(initialCapacity, DEFAULT_LOAD_FACTOR);
    }

  /**
     * Constructs an empty <tt>HashMap</tt> with the specified initial
     * capacity and load factor.
     *
     * @param  initialCapacity the initial capacity
     * @param  loadFactor      the load factor
     * @throws IllegalArgumentException if the initial capacity is negative
     *         or the load factor is nonpositive
     */
     // 指定负载因子 和初始容量
    public HashMap(int initialCapacity, float loadFactor) {
	    //  如果初始容量小于零 非法参数
        if (initialCapacity < 0)
            throw new IllegalArgumentException("Illegal initial capacity: " +
                                               initialCapacity);
        // 如果初始容量 大于  最大容量 还是使用最大容量
        if (initialCapacity > MAXIMUM_CAPACITY)
            initialCapacity = MAXIMUM_CAPACITY;
            // 判断负载因子非法
        if (loadFactor <= 0 || Float.isNaN(loadFactor))
            throw new IllegalArgumentException("Illegal load factor: " +
                                               loadFactor);
  		// 负载因子直接赋值                                             
        this.loadFactor = loadFactor;
        // 调用函数获得容量阈值
        this.threshold = tableSizeFor(initialCapacity);
    }
    /**
     * Returns a power of two size for the given target capacity.
     * 返回给定目标容量的两个大小的幂。
     */
     // 假定传入 10
    static final int tableSizeFor(int cap) {
    	// 9
 	    int n = cap - 1;
        // 0000 1001 无符号右移-> 0000 0100 
        // 再位或 0000 1101
        // 此算法采用移位运算,性能较优 
        // 首先 右移一位+位或 可以保证高位和次高位为1
		n |= n >>> 1;
        // 0000 1101 右移-> 0000 0011
        // 再位或 0000 1111
        // 再右移两位+位或 可以保证高位4位都为1(前面有两位,再加两位)
        n |= n >>> 2;
        // 0000 1111 右移-> 0000 0000
        // 位或 0000 1111
        // 同理 可以保证高8位 为1 (如果没有8位,因为是位或和无符号右移 所以可以保证没有补零)
        n |= n >>> 4;
        // 同理 后面 8+8
        n |= n >>> 8;
        // 16+16 环环相扣 逐步补齐 最高可以补齐32位(此情况32位1 返回默认最大值)
        n |= n >>> 16;
        // 如果最后溢出 则按固定值返回   否则加一返回 (刚好2的幂次)
        return (n < 0) ? 1 : (n >= MAXIMUM_CAPACITY) ? MAXIMUM_CAPACITY : n + 1;        
    }
	 /**
	 	创建一个新的HashMap 随着 Map相同的映射
	 		这个HashMap 创建使用默认值 和初始容量
     * Constructs a new <tt>HashMap</tt> with the same mappings as the
     * specified <tt>Map</tt>.  The <tt>HashMap</tt> is created with
     * default load factor (0.75) and an initial capacity sufficient to
     * hold the mappings in the specified <tt>Map</tt>.
     *
     * @param   m the map whose mappings are to be placed in this map
     * @throws  NullPointerException if the specified map is null
     */
    public HashMap(Map<? extends K, ? extends V> m) {
        this.loadFactor = DEFAULT_LOAD_FACTOR;
        // 将参数map 存入
        putMapEntries(m, false);
    }
    /**
     * The number of key-value mappings contained in this map.
     */
    transient int size;
     /**
     该表在首次使用时和必要调整时初始化。
     分配时，长度始终是2的幂。
     （我们还允许在某些操作中允许长度为零
     目前不需要的自举机制。）
     * The table, initialized on first use, and resized as
     * necessary. When allocated, length is always a power of two.
     * (We also tolerate length zero in some operations to allow
       bootstrapping mechanics that are currently not needed.)
     */
    transient Node<K,V>[] table;
  
    /**
     * The bin count threshold for untreeifying a (split) bin during a
     * resize operation. Should be less than TREEIFY_THRESHOLD, and at
     * most 6 to mesh with shrinkage detection under removal.
     */
    static final int UNTREEIFY_THRESHOLD = 6;
  
     /**
     * Implements Map.putAll and Map constructor
     *	
     * @param m the map
     * @param evict false when initially constructing this map, else
     * true (relayed to method afterNodeInsertion).
     */
    final void putMapEntries(Map<? extends K, ? extends V> m, boolean evict) {
    	// 获得大小 , 此映射中包含的键 - 值映射的数量。
        int s = m.size();
        if (s > 0) { //有数据再加入
            if (table == null) { // pre-size
            	// 如果没有初始化
            	// 通过 除负载因子(0.75) 再加一
                float ft = ((float)s / loadFactor) + 1.0F;
                // 再判断是否超出最大值 然后转换成 int 
                int t = ((ft < (float)MAXIMUM_CAPACITY) ?
                         (int)ft : MAXIMUM_CAPACITY);
                // 判断是否超出阈值
                if (t > threshold)
                	//再调用扩容函数 找到大于这个数值的最小指数幂
                    threshold = tableSizeFor(t);
            }
            // 如果不为空(已经初始化)并且 大于阈值
            else if (s > threshold)
                resize();//调用函数
            // 前面容量调整完后
            //循环遍历m
            for (Map.Entry<? extends K, ? extends V> e : m.entrySet()) {
                K key = e.getKey();
                V value = e.getValue();
                // 存入
                putVal(hash(key), key, value, false, evict);
            }
        }
    }

    /**
    初始化或双精度表大小。
	如果为空，则按照字段阈值中包含的初始容量目标分配。
	否则，因为我们使用的是2的幂展开，所以每个bin中的元素必须保持相同的索引，或者在新表中以2的幂偏移量移动。
     * Initializes or doubles table size.  If null, allocates in
     * accord with initial capacity target held in field threshold.
     * Otherwise, because we are using power-of-two expansion, the
     * elements from each bin must either stay at same index, or move
     * with a power of two offset in the new table.
     *
     * @return the table
     */
    final Node<K,V>[] resize() {
    	// 赋值临时变量
        Node<K,V>[] oldTab = table;
        // 获得容量
        int oldCap = (oldTab == null) ? 0 : oldTab.length;
        // 获得阈值
        int oldThr = threshold;
        // 新值为0
        int newCap, newThr = 0;
        // 如果容量大于0
        if (oldCap > 0) {
        	// 已经到了最大
            if (oldCap >= MAXIMUM_CAPACITY) {// 1 << 30;
            	// 阈值也为最大
                threshold = Integer.MAX_VALUE;// 0x7fffffff;
                return oldTab;//返回
            }
            // 没有到最大 并且 将新容量 = 原始容量 左移(乘二)
            // 新容量要小于最大容量 并且 原始容量 大于等于初始化容量
            else if ((newCap = oldCap << 1) < MAXIMUM_CAPACITY &&
                     oldCap >= DEFAULT_INITIAL_CAPACITY)
                     // 新阈值也左移乘二
                newThr = oldThr << 1; // double threshold
        }
        // (如果容量小于等于零并且)阈值大于0 , 将初始阈值赋值新容量
        else if (oldThr > 0) // initial capacity was placed in threshold
            newCap = oldThr;
        //以上情况都不成立 , 即未指定任何容量和阈值 将默认值替代
        else {               // zero initial threshold signifies using defaults
            newCap = DEFAULT_INITIAL_CAPACITY;
            newThr = (int)(DEFAULT_LOAD_FACTOR * DEFAULT_INITIAL_CAPACITY);//负载因子和默认容量相乘得到新的阈值
        }
        //保证新的阈值不为零 
        if (newThr == 0) {
        	//新阈值为零 新容量必定不为零 所以使用新的容量与负载因子相乘
            float ft = (float)newCap * loadFactor;
            // 容量和新ft 不在范围内则取Integer.MAX_VALUE 
            newThr = (newCap < MAXIMUM_CAPACITY && ft < (float)MAXIMUM_CAPACITY ?
                      (int)ft : Integer.MAX_VALUE);
        }
        // 新阈值 赋值
        threshold = newThr;
        @SuppressWarnings({"rawtypes","unchecked"})
        // 新的数组对象
    	Node<K,V>[] newTab = (Node<K,V>[])new Node[newCap];
        table = newTab;//原始的table  在oldTab 
        //如果原始table 有内容
        if (oldTab != null) {
        	//遍历
            for (int j = 0; j < oldCap; ++j) {
                Node<K,V> e;
                // 如果第j个键值对不为空
                if ((e = oldTab[j]) != null) {
                	// 将原始数组位置置空
                    oldTab[j] = null;
                    // e.next 此处不知道是什么值 , 那么我们看下位于HashMap中的内部静态类Node 的数据结构
                    // 其中next 是自己相同类型的引用 ,  可以猜想应该是类似链表中的下一个引用 
                    // 是否猜想正确 , 我们接着看源码,到存入值时可以得到验证
                    // 如果为空 那么只有一个节点重新计算存入新的数组中
                    if (e.next == null)
                    	//& 操作 : 两个数都转为二进制，然后从高位开始比较，如果两个数都为1则为1，否则为0
                    	// 将e哈希值 位与 新的node数组下标 得到的下标位置将原始对象 e 赋值进去
                    	// 比如:容量16-1(1111) 
                    	// 原始对象hash 9(1001)->1001 hash 8(1000)->1000
                    	// 如果容量15-1(1110)
                    	// 原始对象hash 9(1001)->1000 hash 8(1000)->1000
                    	// 所以这也是为什么数组容量要为2的幂次原因:减少hash碰撞
                        newTab[e.hash & (newCap - 1)] = e;
                    else if (e instanceof TreeNode)
                    //如果不为空,并且类型为TreeNode
                    	//强转成treenode 类型后调用方法split
                        ((TreeNode<K,V>)e).split(this, newTab, j, oldCap);
                    else { // preserve order
                        Node<K,V> loHead = null, loTail = null;
                        Node<K,V> hiHead = null, hiTail = null;
                        Node<K,V> next;
                        do {
                            next = e.next;
                            if ((e.hash & oldCap) == 0) {
                                if (loTail == null)
                                    loHead = e;
                                else
                                    loTail.next = e;
                                loTail = e;
                            }
                            else {
                                if (hiTail == null)
                                    hiHead = e;
                                else
                                    hiTail.next = e;
                                hiTail = e;
                            }
                        } while ((e = next) != null);
                        if (loTail != null) {
                            loTail.next = null;
                            newTab[j] = loHead;
                        }
                        if (hiTail != null) {
                            hiTail.next = null;
                            newTab[j + oldCap] = hiHead;
                        }
                    }
                }
            }
        }
        return newTab;
    }
		/**
			在树上按高低 拆分节点 仅在resize 调用
         * Splits nodes in a tree bin into lower and upper tree bins,
         * or untreeifies if now too small. Called only from resize;
         * see above discussion about split bits and indices.
         *
         * @param map the map
         * @param tab the table for recording bin heads
         * @param index the index of the table being split
         * @param bit the bit of hash to split on
         */
        final void split(HashMap<K,V> map, Node<K,V>[] tab, int index, int bit) {
        	// 当前调用的对象 与 上面传入的this(map) 区分 
            TreeNode<K,V> b = this;//e
            // Relink into lo and hi lists, preserving order
            // 同样初始化临时变量 暂时不知道含义 先往下看
            TreeNode<K,V> loHead = null, loTail = null;
            TreeNode<K,V> hiHead = null, hiTail = null;
            int lc = 0, hc = 0;
            // 从当前的对象 开始往next 遍历直到无下一元素
            // 这里的next 申明 省略了在循环体内声明 直接在for 开始申明一次
            for (TreeNode<K,V> e = b, next; e != null; e = next) {
            	//下一元素赋值
                next = (TreeNode<K,V>)e.next;
                //并且把当前的e next 置空 ,这里就把e 的next链 断开
                e.next = null;
                // 把当前对象的hash与bit(原始table容量)位与(同1为1其余为0)
                // 这里和上面resize中不一样的是没有减1
   				// 比如:容量16(10000) 16-1(1111) 
                // 原始对象hash计算数组位置: 9(1001)->1001
                //	此处hash 9(1001)->00000
                if ((e.hash & bit) == 0) {//所以如果是与数组上元素hash定位相同
                	//这里先将loTail 尾部赋值给当前元素的前驱 
                	//如果尾为空 则为头
                    if ((e.prev = loTail) == null)
                        loHead = e;
                    else//否者往尾部接上
                        loTail.next = e;
                    //保证当前元素在尾部有引用
                    loTail = e;
                    ++lc;//次数加一
                    //这里可以临时看作是一个双向链表操作 ,后面还有后续处理
                }
                else {//如果与hash不同 同样操作 但是接入的是hiTail hiHead 
                    if ((e.prev = hiTail) == null)
                        hiHead = e;
                    else
                        hiTail.next = e;
                    hiTail = e;
                    ++hc;//hiTail 统计次数加一
                    // 这里特别有一个疑问,为什么源码里面加一操作都是前置+ 而不是hc++?
                    // 在查了搜索引擎后有两种说法 ,
                    //  一种说法是前置比后置少寄存器操作,是直接在内存上加一 ,而且后置+相当于 hc+=1 需要多复制一个副本
                    // 还一种则引用了"易变"的说法,减少易变性
                }
            }
            // 这里可以大致了解前面变量含义 高位头尾和长度 - 低位头尾和长度
            if (loHead != null) {//处理低位
                if (lc <= UNTREEIFY_THRESHOLD)//如果小于等于这个非树化阈值6 
                	// 进入看非树化操作,注意:是loHead调用 untreeify 传入参数为hashMap对象
                    tab[index] = loHead.untreeify(map);
                else {//超过阈值树化
                    tab[index] = loHead;
                    if (hiHead != null) // (else is already treeified)
                        loHead.treeify(tab);//如果高位头不为空则还没有树化,否则调用树化,看下树化函数后考虑下,为什么树化函数与高位头为空有关呢
                }
            }
            if (hiHead != null) {
                if (hc <= UNTREEIFY_THRESHOLD)
                    tab[index + bit] = hiHead.untreeify(map);
                else {
                    tab[index + bit] = hiHead;
                    if (loHead != null)
                        hiHead.treeify(tab);
                }
            }
        }
		/**返回一个非TreeNode列表
         * Returns a list of non-TreeNodes replacing those linked from
         * this node.
         */
        final Node<K,V> untreeify(HashMap<K,V> map) {
            Node<K,V> hd = null, tl = null;
            // q首先为当前调用node对象,刚才loHead.untreeify(map);分析是loHead
            // 那么为什么TreeNode可以赋值给Node?(TreeNode extends LinkedHashMap.Entry  extends HashMap.Node)
            for (Node<K,V> q = this; q != null; q = q.next) {
                Node<K,V> p = map.replacementNode(q, null);
                if (tl == null)//同样是类似链表操作 那么tl是tail hd 是head
                    hd = p;//第一次赋值给头
                else //否则往尾部添加形成链
                    tl.next = p;
                tl = p;//再将尾改成当前的
            }
            return hd;// 最后返回这个链表
        }
        // For conversion转换 from TreeNodes to plain nodes 
	    Node<K,V> replacementNode(Node<K,V> p, Node<K,V> next) {
	    // 直接new 返回 将 p 的值转换
	        return new Node<>(p.hash, p.key, p.value, next);
	    }
	 /**
	 	位于HashMap中的内部静态类
     * Basic hash bin node, used for most entries.  (See below for
     * TreeNode subclass, and in LinkedHashMap for its Entry subclass.)
     */
    static class Node<K,V> implements Map.Entry<K,V> {
        final int hash;
        final K key;
        V value;
        // next 是下一个的引用
        Node<K,V> next;

        Node(int hash, K key, V value, Node<K,V> next) {
            this.hash = hash;
            this.key = key;
            this.value = value;
            this.next = next;
        }

        public final K getKey()        { return key; }
        public final V getValue()      { return value; }
        public final String toString() { return key + "=" + value; }

        public final int hashCode() {
            return Objects.hashCode(key) ^ Objects.hashCode(value);
        }

        public final V setValue(V newValue) {
            V oldValue = value;
            value = newValue;
            return oldValue;
        }

        public final boolean equals(Object o) {
            if (o == this)
                return true;
            if (o instanceof Map.Entry) {
                Map.Entry<?,?> e = (Map.Entry<?,?>)o;
                if (Objects.equals(key, e.getKey()) &&
                    Objects.equals(value, e.getValue()))
                    return true;
            }
            return false;
        }
    }




  /**
     * Implements Map.put and related methods
     *
     * @param hash hash for key
     * @param key the key
     * @param value the value to put
     * @param onlyIfAbsent if true, don't change existing value
     * @param evict if false, the table is in creation mode.
     * @return previous value, or null if none
     */
    final V putVal(int hash, K key, V value, boolean onlyIfAbsent,
                   boolean evict) {
        Node<K,V>[] tab; Node<K,V> p; int n, i;
        if ((tab = table) == null || (n = tab.length) == 0)
            n = (tab = resize()).length;
        if ((p = tab[i = (n - 1) & hash]) == null)
            tab[i] = newNode(hash, key, value, null);
        else {
            Node<K,V> e; K k;
            if (p.hash == hash &&
                ((k = p.key) == key || (key != null && key.equals(k))))
                e = p;
            else if (p instanceof TreeNode)
                e = ((TreeNode<K,V>)p).putTreeVal(this, tab, hash, key, value);
            else {
                for (int binCount = 0; ; ++binCount) {
                    if ((e = p.next) == null) {
                        p.next = newNode(hash, key, value, null);
                        if (binCount >= TREEIFY_THRESHOLD - 1) // -1 for 1st
                            treeifyBin(tab, hash);
                        break;
                    }
                    if (e.hash == hash &&
                        ((k = e.key) == key || (key != null && key.equals(k))))
                        break;
                    p = e;
                }
            }
            if (e != null) { // existing mapping for key
                V oldValue = e.value;
                if (!onlyIfAbsent || oldValue == null)
                    e.value = value;
                afterNodeAccess(e);
                return oldValue;
            }
        }
        ++modCount;
        if (++size > threshold)
            resize();
        afterNodeInsertion(evict);
        return null;
    }


```



